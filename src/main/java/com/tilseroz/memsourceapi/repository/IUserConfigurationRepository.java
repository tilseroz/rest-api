package com.tilseroz.memsourceapi.repository;


import com.tilseroz.memsourceapi.model.UserConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author David Tilšer
 */
public interface IUserConfigurationRepository extends JpaRepository<UserConfiguration, Long>{
    
}
