package com.tilseroz.memsourceapi.controller;

import com.tilseroz.memsourceapi.model.UserConfiguration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.tilseroz.memsourceapi.service.ITokenService;
import com.tilseroz.memsourceapi.service.IUserConfigurationService;

/**
 *
 * @author David Tilšer
 */
@Controller
public class TokenController {

    @Autowired
    ITokenService tokenService;

    @Autowired
    IUserConfigurationService userConfigurationService;

    @RequestMapping(value = "/memsourceuser/getToken.htm", method = RequestMethod.POST)
    public @ResponseBody
    String getToken(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserConfiguration userConfiguration = new UserConfiguration(username, password);
        userConfigurationService.saveUserConfiguration(userConfiguration);
        return tokenService.getToken(userConfiguration);
    }
}
