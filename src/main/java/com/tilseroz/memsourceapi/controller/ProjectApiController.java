package com.tilseroz.memsourceapi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author David Tilšer
 */
@Controller
public class ProjectApiController {

    @RequestMapping("getMyProjects")
    public String viewMyProjects() {
        return "index";
    }
    
}
