package com.tilseroz.memsourceapi.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import lombok.Data;
import javax.persistence.Id;

/**
 *
 * @author David Tilšer
 */
@Data
@Entity
public class UserConfiguration implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;

    public UserConfiguration() {
    }

    public UserConfiguration(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
