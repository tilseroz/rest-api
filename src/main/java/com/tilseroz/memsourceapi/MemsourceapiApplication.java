package com.tilseroz.memsourceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemsourceapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemsourceapiApplication.class, args);
	}

}
