package com.tilseroz.memsourceapi.service;

import com.tilseroz.memsourceapi.model.UserConfiguration;
import com.tilseroz.memsourceapi.repository.IUserConfigurationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author David Tilšer
 */
@Service
@Scope("prototype")
public class UserConfigurationService implements IUserConfigurationService {

    @Autowired
    IUserConfigurationRepository userConfigurationRepository;

    @Override
    public void saveUserConfiguration(UserConfiguration userConfiguration) {
        userConfigurationRepository.save(userConfiguration);
    }

}
