package com.tilseroz.memsourceapi.service;

import com.tilseroz.memsourceapi.model.UserConfiguration;
import com.tilseroz.memsourceapi.model.Token;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author David Tilšer
 */
@Service
@Scope("prototype")
public class TokenService implements ITokenService {

    private static final String LOGIN_URI = "https://cloud.memsource.com/web/api2/v1/auth/login";
    private Token token;
    private ResponseEntity<Token> responseLoginApi;

    @Override
    public String getToken(UserConfiguration userConfiguration) {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> payload = new HashMap<>();
        payload.put("userName", userConfiguration.getUsername());
        payload.put("password", userConfiguration.getPassword());
        try {
            responseLoginApi = restTemplate.postForEntity(LOGIN_URI, payload, Token.class);
        } catch (HttpClientErrorException e) {
            HttpStatus status = e.getStatusCode();
            if (status != HttpStatus.NOT_FOUND) {
                throw e;
            }
        }
        return getTokenFromResponseEntity(responseLoginApi);
    }

    private String getTokenFromResponseEntity(ResponseEntity<Token> responseLoginApi) {
        if (responseLoginApi != null && HttpStatus.OK.equals(responseLoginApi.getStatusCode())) {
            token = responseLoginApi.getBody();
            return token.getToken();
        } else {
            throw new RuntimeException("Call MemsourceApiLogin failed. Rest API will probably be inactive.");
        }
    }
}
