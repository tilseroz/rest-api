package com.tilseroz.memsourceapi.service;

import com.tilseroz.memsourceapi.model.UserConfiguration;

/**
 *
 * @author David Tilšer
 */
public interface IUserConfigurationService {

    void saveUserConfiguration(UserConfiguration userConfiguration);

}
